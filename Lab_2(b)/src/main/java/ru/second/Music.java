package ru.second;

import java.util.List;

public interface Music {
    List<String> getSongs();
}
