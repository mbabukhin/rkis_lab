package ru.second;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MusicPlayer {

    @PostConstruct
    public void init() {
        System.out.println("Плеер включен");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Плеер выключен");
    }
    @Value("${musicPlayer.name}")
    private String name;
    @Value("${musicPlayer.volume}")
    private int volume;

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    private ClassicalMusic classicalMusic;
    private RockMusic rockMusic;
    private RapMusic rapMusic;
    private PopMusic popMusic;


    public MusicPlayer(ClassicalMusic classicalMusic, RockMusic rockMusic, RapMusic rapMusic, PopMusic popMusic) {
        this.classicalMusic = classicalMusic;
        this.rockMusic = rockMusic;
        this.rapMusic = rapMusic;
        this.popMusic = popMusic;
    }

    /**
     * Фабричный метод обновления данных вагона
     * @return Плеер с обновлённой информацией о композициях
     */
    public MusicPlayer musicPlayer(MusicPlayer musicPlayer,
                                   ClassicalMusic classicalMusic, RockMusic rockMusic,
                                   RapMusic rapMusic, PopMusic popMusic) {
        return new MusicPlayer(classicalMusic, rockMusic, rapMusic,popMusic);
    }


    //Метод проигрывания случайной музыки - имитация при включении
    public void playMusic(MusicGenre genre) {
        Random random = new Random();

        // случайное целое число между 0 и 3
        int randomNumber = random.nextInt(4);

        if (genre == MusicGenre.CLASSICAL) {
            // случайная классическая песня
            System.out.println(classicalMusic.getSongs().get(randomNumber));
        }
        if (genre == MusicGenre.ROCK) {
            // случайная рок песня
            System.out.println(rockMusic.getSongs().get(randomNumber));
        }
        if (genre == MusicGenre.POP) {
            // случайная поп песня
            System.out.println(popMusic.getSongs().get(randomNumber));
        }
        if (genre == MusicGenre.RAP) {
            // случайная рэп песня
            System.out.println(rapMusic.getSongs().get(randomNumber));
        }



    }
}
