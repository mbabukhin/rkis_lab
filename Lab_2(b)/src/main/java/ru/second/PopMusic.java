package ru.second;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


public class PopMusic implements Music{

    List<String> songs = new ArrayList<>();
    {
        songs.add("Lady gaga - ra ra ra");
        songs.add("Artem - da da da");
        songs.add("Михаил Круг -  Владимирский централ");

    }
    @Override
    public List<String> getSongs() {
        return songs;
    }
}
