package ru.second;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class RapMusic implements Music{
    private List<String> songs = new ArrayList<>();

    {
        songs.add("OXXYMIRON - Ойда");
        songs.add("50 cent - candy shop");
        songs.add("Eminem - Beautiful");
    }


    @Override
    public List<String> getSongs() {
        return songs;
    }
}
