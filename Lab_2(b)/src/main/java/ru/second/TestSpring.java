package ru.second;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new
                AnnotationConfigApplicationContext(SpringConfig.class);

        MusicPlayer musicPlayer = context.getBean(MusicPlayer.class);
        musicPlayer.playMusic(MusicGenre.RAP);
        System.out.println("Громкость плеера: " + musicPlayer.getVolume());
        System.out.println("Производитель: " + musicPlayer.getName());

        context.close();
    }

}