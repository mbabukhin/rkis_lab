package ru.second;

import java.util.ArrayList;
import java.util.List;
public class MusicPlayer {
    private Music music;

    public MusicPlayer(Music music) {
        this.music = music;
    }

    public void PlayMusic() {
        System.out.println("Playing: " + music.getSong());
    }
    public void StopMusic() {
        System.out.println("Stop playing music...");
    }
}
